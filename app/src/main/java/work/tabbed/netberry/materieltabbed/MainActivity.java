package work.tabbed.netberry.materieltabbed;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fragmentManager = getSupportFragmentManager();
        viewPager = (ViewPager)findViewById(R.id.viewPage);
        viewPager.setAdapter(new MyAdapter(fragmentManager));

    }
}
class MyAdapter extends FragmentPagerAdapter
{


    public MyAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if(position==0)
        {
            fragment = new FragmentA();
        }
        else if(position==1)
        {
            fragment = new FragmentB();
        }
        else if(position==2)
        {
            fragment = new FragmentC();
        }
        else if(position==3)
        {
            fragment = new FragmentD();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if(position==0) { return "TAB 1"; }
        if(position==1) { return "TAB 2"; }
        if(position==2) { return "TAB 3"; }
        if(position==3) { return "TAB 4"; }
        return null;
    }
}
